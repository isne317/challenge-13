#include<iostream>
#include<list>
#include<string>

using namespace std;

class Student
{
	private:
		string name;
		unsigned int id;
		
	public:
		string getName()
		{
			return name;	
		}
		
		unsigned int getID()
		{
			return id;	
		}
		
		void setName(string input)
		{
			name = input;
		}
		
		void setID(unsigned int input)
		{
			id = input;
		}
};

list<Student> hashTable[50];

void addElement(unsigned int, string);
string removeElementByID(unsigned int);
string searchByID(unsigned int);
unsigned int searchByName(string);
void showElementsID(unsigned int);
void showAll();

int main()
{
	/*
		state:
			0: main menu
			1: add
			2: remove by ID
			3: search by name
			4: search by ID
			5: show elements with the certain hash ID
			6: show all
			7: exit
	*/
	int state = 0;
	int nElements = 0;
	unsigned int intInput;
	string stringInput;
	while(state != 7)
	{
		if(state == 0)
		{
			system("cls");
			if(nElements <= 1) {cout << "There are currently " << nElements << " student in the table." << endl;}
			else {cout << "There are currently " << nElements << " students in the table." << endl;}
			cout << "-------------------------------------------------" << endl;
			cout << " 1: Add a student." << endl;
			cout << " 2: Remove a student by ID." << endl;
			cout << " 3: Search by name." << endl;
			cout << " 4: Search by ID." << endl;
			cout << " 5: Show student(s) with the same last 2 digits." << endl;
			cout << " 6: Show all students." << endl;
			cout << " 7: Exit." << endl;
			cout << "-------------------------------------------------" << endl;
			cout << "Please input a number and press enter to operate: ";
			cin >> intInput;
			while(intInput < 1 || intInput > 7)
			{
				cout << "INVALID INPUT." << endl;
				cout << "Please input a number and press enter to operate: ";
				cin >> intInput;
			}
			state = intInput;
		}
		else if(state == 1)
		{
			cout << "Please enter ID: ";
			cin >> intInput;
			cout << "Please enter name: ";
			cin.ignore();
			getline(cin, stringInput);
			addElement(intInput, stringInput);
			nElements++;
			cout << "The student is added to the table #" << (intInput % 50) << endl;
			cout << "Press enter to continue";
			cin.ignore();
			cin.get();
			state = 0;
		}
		else if(state == 2)
		{
			cout << "Please enter ID: ";
			cin >> intInput;
			stringInput = removeElementByID(intInput); // I RECYCLED the variable.
			if(stringInput != "N/A")
			{
				nElements--;
				cout << "The student " << stringInput << " is removed!" << endl;
				cout << "Press enter to continue";
				cin.ignore();
				cin.get();
				state = 0;
			}
			else
			{
				cout << "Student not found!" << endl;
				cout << "Press enter to continue";
				cin.ignore();
				cin.get();
				state = 0;	
			}
		}
		else if(state == 3)
		{
			cout << "Please enter name: ";
			cin.ignore();
			getline(cin, stringInput);
			intInput = searchByName(stringInput);
			if(intInput != 0)
			{
				cout << "Result ID: " << intInput;
				cout << "Press enter to continue";
				cin.ignore();
				cin.get();
				state = 0;
			}
			else
			{
				cout << "Student not found!" << endl;
				cout << "Press enter to continue";
				cin.ignore();
				cin.get();
				state = 0;
			}
		}
		else if(state == 4)
		{
			cout << "Please enter ID: ";
			cin >> intInput;
			stringInput = searchByID(intInput);
			if(stringInput != "N/A")
			{
				cout << "Result name: " << stringInput << endl;
				cout << "Press enter to continue";
				cin.ignore();
				cin.get();
				state = 0;
			}
			else
			{
				cout << "Student not found!" << endl;
				cout << "Press enter to continue";
				cin.ignore();
				cin.get();
				state = 0;	
			}
		}
		else if(state == 5)
		{
			cout << "Please enter ID (1-50, 51-60 are treated as 1-10): ";
			cin >> intInput;
			while(intInput < 1 || intInput > 60)
			{
				cout << "INVALID INPUT!" << endl;
				cout << "Please enter ID (1-50, 51-60 are treated as 1-10): ";
				cin >> intInput;
			}
			showElementsID(intInput);
			cout << "Press enter to continue";
			cin.ignore();
			cin.get();
			state = 0;
		}
		else if(state == 6)
		{
			showAll();
			cout << "Press enter to continue";
			cin.ignore();
			cin.get();
			state = 0;	
		}
	}
	cout << "Have a nice day!" << endl;
	return 0;
}

void addElement(unsigned int id, string name)
{
	/*
		Add an element to the table.
		Basically create a Student object here and push it.
	*/
	unsigned int hashAddress = (id - 1) % 50;
	Student input;
	input.setName(name);
	input.setID(id);
	hashTable[hashAddress].push_back(input);
}

string removeElementByID(unsigned int id)
{
	/*
		Remove an element off the table based on the ID.
		Only exact ID only.
	*/
	list<Student>::iterator ite;
	string name;
	for(ite = hashTable[(id - 1) % 50].begin(); ite != hashTable[(id - 1) % 50].end(); ite++)
	{
		if((*ite).getID() == id)
		{
			name = (*ite).getName();
			hashTable[(id - 1) % 50].erase(ite);
			return name;
		}
	}
	return "N/A";	
}

string searchByID(unsigned int id)
{
	/*
		Search for name using the ID.
		If not found, return N/A.
	*/
	list<Student>::iterator ite;
	for(ite = hashTable[(id - 1) % 50].begin(); ite != hashTable[(id - 1) % 50].end(); ite++)
	{
		if((*ite).getID() == id)
		{
			return (*ite).getName();
		}
	}
	return "N/A";
}

unsigned int searchByName(string name)
{
	/*
		Search for ID using name.
		If not found, return 0.
	*/
	list<Student>::iterator ite;
	for(int id = 0; id < 50; id++)
	{
		for(ite = hashTable[id].begin(); ite != hashTable[id].end(); ite++)
		{
			if((*ite).getName() == name)
			{
				return (*ite).getID();
			}
		}
	}
	return 0;	
}

void showElementsID(unsigned int hash)
{
	/*
		Show all elements in the table with the exact hash, one element for each line.
	*/
	list<Student>::iterator ite;
	cout << "The list of students with \"" << hash << "\"" << endl;
	cout << "---------------------------------------------------" << endl;
	for(ite = hashTable[hash - 1].begin(); ite != hashTable[hash - 1].end(); ite++)
	{
		cout << (*ite).getID() << " | " << (*ite).getName() << endl;
	}
	cout << "---------------------------------------------------" << endl;
}

void showAll()
{
	/*
		Show ALL elements in the table, starts from hash 1 to 50.
	*/
	list<Student>::iterator ite;
	cout << "The list of students." << endl;
	cout << "---------------------------------------------------" << endl;
	for(int id = 1; id <= 50; id++)
	{
		for(ite = hashTable[(id % 51) - 1].begin(); ite != hashTable[(id % 51) - 1].end(); ite++)
		{
			cout << (*ite).getID() << " | " << (*ite).getName() << endl;
		}
	}
	cout << "---------------------------------------------------" << endl;
}
